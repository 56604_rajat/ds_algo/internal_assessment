import java.util.Scanner;

public class D4_56604_Rajat {
    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);
        LinkedList l1 = new LinkedList();
        boolean status = true;
        while(status)
        {
            System.out.println("---------------------------------------------------------");
            System.out.println("Enter your choice\n 0.Exit\n 1.Add Last Vehicle object in to the list\n 2.add Fisrt Vehicle object in to the list\n 3.delete Last Vehicle object from the list\n 4.delete First Vehicle object from the list\n 5.search Vehicle by No\n 6.print all Vehicle details\n 7.print Vehicle list in reverse order\n 8.Sort Vehicle List by price");
            int choice = sc.nextInt();
            switch(choice)
            {
                case 0:
                status = false;
                break;
                case 1:
                System.out.println("Enter Details");
                System.out.println("Company: ");
                String company = sc.next();
                System.out.println("Model: ");
                String model = sc.next();
                System.out.println("Type: ");
                String type = sc.next();
                System.out.println("Price: ");
                double price = sc.nextDouble();
                System.out.println("No: ");
                int no = sc.nextInt();
                System.out.println("Color: ");
                String color = sc.next();
                l1.addNodeAtLastPosition(new Vehical(company,model,type,price,no,color));
                System.out.println("Vehicle added at last position");
                break;
                case 2:
                System.out.println("Enter Details");
                System.out.println("Company: ");
                String company1 = sc.next();
                System.out.println("Model: ");
                String model1 = sc.next();
                System.out.println("Type: ");
                String type1 = sc.next();
                System.out.println("Price: ");
                double price1 = sc.nextDouble();
                System.out.println("No: ");
                int no1 = sc.nextInt();
                System.out.println("Color: ");
                String color1 = sc.next();
                l1.addNodeAtFirstPosition(new Vehical(company1,model1,type1,price1,no1,color1));
                System.out.println("Vehicle added at first position");
                break;
                case 3:
                l1.deleteNodeAtLastPosition();
                System.out.println("Last postition Vehical deleted successfully");
                break;
                case 4:
                l1.deleteNodeAtFirstPosition();
                System.out.println("First postition Vehical deleted successfully");
                break;
                case 5:
                System.out.println("Enter no.");
                int number = sc.nextInt();
                l1.searchByNo(number);
                break;
                case 6:
                l1.displayLinkedList();
                break;
                case 7:
                l1.reversedLinkedList();
                break;
                case 8:
                l1.sortByPrice();
                System.out.println("Sorted successfully");
                break;
            }
        }
        sc.close();
    }
}

class Vehical 
{
    private String company;
    private String model;
    private String type;
    private double price;
    private int no;
    private String color;

    public Vehical(String company, String model, String type, double price, int no, String color)
    {
        this.company = company;
        this.model = model;
        this.type = type;
        this.price = price;
        this.no = no;
        this.color = color;
    }

    public String getCompany()
    {
        return this.company;
    }

    public String getModel()
    {
        return this.model;
    }

    public String getType()
    {
        return this.type;
    }

    public double getPrice()
    {
        return this.price;
    }

    public int getNo()
    {
        return this.no;
    }

    public String getColor()
    {
        return this.color;
    }
    
    public void displayInfo()
    {
        System.out.println("Company : "+this.company+", Model : "+this.model+", Type : "+this.type+", Price : Rs."+this.price+", no : "+this.no+", Color : "+this.color);
    }
}

class LinkedList{
    static class Node{
        private Vehical vehical;
        private Node prev;
        private Node next;

        public Node(Vehical vehical){
            this.vehical = vehical;
            this.prev = null;
            this.next = null;
        }

    }

    private Node head;
    private int nodesCount;
    
    public LinkedList(){
        head = null;
        nodesCount=0;
    }

    public boolean isListEmpty( ){
        return ( head == null );
    }

    public int getNodesCount( ){
        return ( this.nodesCount );
    }

    public void addNodeAtLastPosition(Vehical vehi){
        Node newNode = new Node(vehi);
        if( head == null ){
            head = newNode;
            newNode.next = head;
            newNode.prev = head;
            nodesCount++;
        }else{
            newNode.prev = head.prev;
            newNode.next = head;
            head.prev.next = newNode;
            head.prev = newNode;
            nodesCount++;
        }
    }

    public void addNodeAtFirstPosition(Vehical vehi){
        Node newNode = new Node(vehi);
        if( head == null ){
            head = newNode;
            newNode.next = head;
            newNode.prev = head;
            nodesCount++;
        }else{
            newNode.prev = head.prev;
            newNode.next = head;
            head.prev.next = newNode;
            head.prev = newNode;
            head  = newNode;
            nodesCount++;
        }
    }

    public void deleteNodeAtLastPosition(){
        
        if( !isListEmpty() ){
            if( head == head.next ){
                head = null;
                nodesCount--;
            }else{
                head.prev.prev.next = head;
                head.prev = head.prev.prev;
                nodesCount--;
            }
        }
        else{
            System.out.println("List is empty");
        }
    }

    public void deleteNodeAtFirstPosition(){
        
        if( !isListEmpty() ){
            if( head == head.next ){
                head = null;
                nodesCount--;
            }else{
                head.prev.next = head.next;
                head.next.prev = head.prev;
                head = head.next;
                nodesCount--;
            }
        }
        else{
            System.out.println("List is empty");
        }
    }

    public void searchByNo(int no)
    {
        Node trav = head;
        do{
            if(trav.vehical.getNo()==no)
            {
                trav.vehical.displayInfo();
            }
            trav = trav.next;
        }while( trav != head );
    }

    public void displayLinkedList( ){
        
        if( !isListEmpty() ){
            
            Node trav = head;
          
            do{
                trav.vehical.displayInfo();
                trav = trav.next;
            }while( trav != head );
            System.out.println("no. of nodes in a list are : "+getNodesCount());

        }else
            System.out.println("list is empty !!!");
    }

    public void reversedLinkedList()
    {
        Node trav = head.prev; 
            System.out.print("list in a backward dir is : ");
            do{
                trav.vehical.displayInfo();
                trav = trav.prev;//move trav towards it prev node
            }while( trav != head.prev );
    }

    public void sortByPrice()
    {
        if(head==null)
            System.out.println("List is Empty");
        else
        {
            Node trav = head, index = null;
            Vehical temp;
            do{
                index = trav.next;
                while(index != head)
                {
                    if (trav.vehical.getPrice() > index.vehical.getPrice()) {
                        temp = trav.vehical;
                        trav.vehical = index.vehical;
                        index.vehical = temp;
                    }
 
                    index = index.next;
                }
                trav = trav.next;
            }while(trav!=head);
        }

    }

}